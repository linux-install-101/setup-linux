echo "Setting up config"
mkdir ~/Utils
(cd ~/Utils &&
rm -rf config &&
git clone https://gitlab.com/linux-install-101/config.git &&
chmod +x ./config/install.sh &&
(cd config && ./install.sh) &&
echo "Setting up gnome theme" &&
rm -rf gnome-theme &&
git clone https://gitlab.com/linux-install-101/gnome-theme.git &&
chmod +x ./gnome-theme/install.sh &&
(cd gnome-theme && ./install.sh) &&
echo "Setting up icon dotfiles" &&
rm -rf icon-dotfiles &&
git clone https://gitlab.com/linux-install-101/icon-dotfiles.git &&
chmod +x ./icon-dotfiles/install.sh &&
(cd icon-dotfiles && ./install.sh) &&
rm -rf setup-linux &&
git clone https://gitlab.com/linux-install-101/setup-linux.git &&
chmod +x ./setup-linux/install.sh &&
rm -rf install-my-applications &&
git clone https://gitlab.com/linux-install-101/install-my-applications.git &&
chmod +x ./install-my-applications/install.sh &&
(cd install-my-applications && ./install.sh) &&
echo "Done"
)
